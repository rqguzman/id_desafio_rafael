class CreateProdutos < ActiveRecord::Migration[5.2]
  def change
    create_table :produtos do |t|
      t.string :nome
      t.text :descricao
      t.integer :preco
      t.boolean :disponibilidade, default: true

      t.timestamps
    end
  end
end
