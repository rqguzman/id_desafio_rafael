Rails.application.routes.draw do
  resources :line_items
  resources :reservas
  resources :produtos

  devise_for :users, controlers: {
    registrations: 'registrations'
  }

  root 'pages#home'
  
  get '/about', to: 'pages#about'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
