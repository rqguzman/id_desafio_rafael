module IconsHelper
  FONTAWESOME_ICONS = {
    user:           'user',
    item:           'shopping-basket',
    category:       'tag',
    categories:     'tags',
    email:          'envelope',
    password:       'lock',
    logout:         'sign-out',
    login:          'sign-in',
    signup:         'user-plus',
    edit:           'pencil',
    group_creator:  'gavel',
    # ---content removed for brevity---
  }

  def icon_for(name, options = {})
    options[:title] ||= name.to_s.humanize
    options[:alt]   ||= "#{options[:title]}"
    icon FONTAWESOME_ICONS[name], options
  end

  def input_group_addon_icon(name, options = {})
    content_tag :span, class: 'input-group-addon' do
      icon_for name, options
    end
  end
end