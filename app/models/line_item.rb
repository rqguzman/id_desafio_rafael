class LineItem < ApplicationRecord
  belongs_to :produto
  belongs_to :reserva

  def preco_total
    produto.preco.to_i * quantity.to_i    
  end
end
