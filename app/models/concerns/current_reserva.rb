module CurrentReserva

  private

  def set_reserva
    @reserva = Reserva.find(session[:reserva_id])
  rescue ActiveRecord::RecordNotFound
    @reserva = Reserva.create
    session[:reserva_id] = @reserva.id
  end
end