class Reserva < ApplicationRecord
  has_many :line_items, dependent: :destroy

  def add_produto(produto)
    current_item = line_items.find_by(produto_id: produto.id)
    if current_item
      current_item.increment(:quantity)
    else
      current_item = line_items.buil(produto_id: produto_id)
    end
    current_item
  end

  def preco_total
    line_items.to_a.sum { |item| item.preco_total }
  end
  
end
