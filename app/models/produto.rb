class Produto < ApplicationRecord

  before_destroy :not_referenced_by_any_line_item
  mount_uploader :image, ImageUploader
  serialize :image, JSON # Para o caso de usar SQLite
  belongs_to :user, optional: true
  has_many :line_items

  validates :nome, :preco, presence: true
  validates :descricao, length: { maximum: 1000, too_long: "%{count} caracteres é o tamanho máximo permitido." }
  validates :nome, length: { maximum: 140, too_long: "%{count} caracteres é o tamanho máximo permitido." }
  validates :preco, numericality: { only_integer: true },  length: { maximum: 7 }

  private

  def not_referenced_by_any_line_item
    unless line_items.empty?
      errors.add(:base, "Itens de pedido presentes")
      throw :abort
    end
  end

end
