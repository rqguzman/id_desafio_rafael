class Ability
  include CanCan::Ability
  def initialize(user)
    
    user ||= User.new # guest user (not logged in)
      can :read, :all
      can :read, Produto do |produto|
        produto.try(:user) == user
    end

    if user.admin?
      can :manage, :all
    else
      can [:read, :update], User
      can :read, Produto
    end
  end
end
