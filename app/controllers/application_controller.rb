class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include CurrentReserva
  before_action :set_reserva

  before_action :authenticate_user!

  before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, notice: "Você não possui as permissões necessárias para acessar esta página."
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:admin, :email,
                                                               :password, 
                                                               :password_confirmation)}

    devise_parameter_sanitizer.permit(:account_update) { |u| u.permit(:admin, 
                                                                      :email,
                                                                      :password,
                                                                      :password_confirmation,
                                                                      :current_password)}
  end
  
end
