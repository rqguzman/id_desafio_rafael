class PagesController < ApplicationController
  def home
    if current_user
      redirect_to produtos_path
    end
    @produtos = Produto.last(4)
  end

  def about
  end
end
